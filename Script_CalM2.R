# Start ------------------------------------------------------------------------
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

rm(list = ls())

# libraries --------------------------------------------------------------------
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
library(tidyverse)
library(finalfit)
library(googlesheets4)
library(gsheet)
library(gtsummary)
library(ggpubr)

##  Function -------------------------------------------------------------------

add_total <- function(dta, totalcols = 2:3) {
  dta2 <- bind_rows(dta, apply(dta[, totalcols, drop = F], 2, sum, na.rm = TRUE))
  dta2[, -totalcols] <- lapply(dta2[, -totalcols], as.character)
  dta2[nrow(dta2), -totalcols] <- ""
  dta2[nrow(dta2), 1] <- "Total"
  dta2
}

unfill_vec <-
  function(x, direction = c("down", "up")) {
    if (direction[1] == "down") {
      x[which(x[-1] == x[1:(length(x) - 1)]) + 1] = ""
    } else {
      x = rev(unfill(rev(x), direction = "down"))
    }
    x
  }

linesep <- 
  function(x, y = character()){
    if(!length(x))
      return(y)
    linesep(x[-length(x)], c(rep('', x[length(x)]-1), '\\addlinespace', y))  
  }

# system date
Date_graph <- Sys.setlocale("LC_TIME", "English")
## -----------------------------------------------------------------------------
path_URL <- "https://docs.google.com/spreadsheets/d/1LVXyKOh7QZCJ3KoQDVIq8gNVQEpQZNSnxQzFTkzXDYQ/edit?usp=sharing"


data_CalM <- read_sheet(path_URL, sheet = "data", skip = 1)

# reorganization of the database
colname <- c("record_id", "study", "study_year", "pid", "sf", "random", "sex" , "yob", "age", "weight",
             "height", "bmi", "malaria_d0", "loa_d0", "mens_d0", "schisto_d0", "wbc_d0", "rbc_d0", "hb_d0",
             "plt_d0", "lymph_d0","neu_d0", "monocy_d0", "eos_d0", "basoph_d0", "sudium_d0",
             "potas_d0", "chlorid_d0", "calsum_d0", "magnes_d0", "malaria_d7", "wbc_d7", "rbc_d7",
             "hb_d7", "plt_d7", "lymph_d7"," neu_d7", "monocy_d7", "eos_d7", "basoph_d7", "sudium_d7",
             "potas_d7", "chlorid_d7", "calsum_d7", "magnes_d7", "malaria_d14", "wbc_d14", "rbc_d14",
             "hb_d14", "plt_d14", "lymph_d14"," neu_d14", "monocy_d14", "eos_d14", "basoph_d14",
             "sudium_d14", "potas_d14", "chlorid_d14", "calsum_d14", "magnes_d14", "malaria_d28",
             "wbc_d28", "rbc_d28", "hb_d28", "plt_d28", "lymph_d28"," neu_d28", "monocy_d28",
             "eos_d28", "basoph_d28",  "sudium_d28", "potas_d28", "chlorid_d28", "calsum_d28",
             "magnes_d28")

names(data_CalM) <- colname

dataset <-
  data_CalM %>% 
  mutate_at(vars(wbc_d0, basoph_d0), ~(as.numeric(.))) %>% 
  filter(!is.na(malaria_d0)) %>% 
  mutate_at(vars(weight, height, malaria_d0, malaria_d14, malaria_d28), ~(as.numeric(.))) %>% 
  mutate(record_id = case_when(record_id < 10 ~ paste0("CalME00", record_id),
                               record_id < 100 ~ paste0("CalME0", record_id)),
         malaria_0 = if_else(malaria_d0 > 0, "Positive", "Negative"),
         malaria_14 = if_else(malaria_d14 > 0, "Positive", "Negative"),
         malaria_28 = if_else(malaria_d28 > 0, "Positive", "Negative"),
         sex = case_when(sex == "M" ~ "Male",
                         sex == "F" ~ "Female"),
         age_group = case_when(age <= 5 ~ "0-6",
                               age <= 10 ~ "7-12",
                               age > 12 ~ "13-70"),
         bmi_cat = case_when(bmi < 18.5 ~ "Underweight < 18.5",
                             bmi < 25 ~ "Normal Weight 18.5-24.9",
                             bmi < 30 ~ "Overweight 25-29.9",
                             bmi >= 30 ~ "Obesity > 30"),
         plasmod_0 = case_when(malaria_d0 == 0 ~ "No parasitemia",
                               malaria_d0 <= 1000 ~ "Low parasitaemia (1-1000 parasites/ul)",
                               malaria_d0 <= 10000 ~ "Medium parasitaemia (1001-10000 parasites/ul)",
                               malaria_d0 <= 100000 ~ "Moderatly high parasitaemia (10001-100000 parasites/ul)",
                               malaria_d0 > 100000 ~ "High parasitaemia (> 100000 parasites/ul)"),
         plasmod_14 = case_when(malaria_d14 == 0 ~ "No parasitemia",
                               malaria_d14 <= 1000 ~ "Low parasitaemia (1-1000 parasites/ul)",
                               malaria_d14 <= 10000 ~ "Medium parasitaemia (1001-10000 parasites/ul)",
                               malaria_d14 <= 100000 ~ "Moderatly high parasitaemia (10001-100000 parasites/ul)",
                               malaria_d14 > 100000 ~ "High parasitaemia (> 100000 parasites/ul)"),
         plasmod_28 = case_when(malaria_d28 == 0 ~ "No parasitemia",
                                malaria_d28 <= 1000 ~ "Low parasitaemia (1-1000 parasites/ul)",
                                malaria_d28 <= 10000 ~ "Medium parasitaemia (1001-10000 parasites/ul)",
                                malaria_d28 <= 100000 ~ "Moderatly high parasitaemia (10001-100000 parasites/ul)",
                                malaria_d28 > 100000 ~ "High parasitaemia (> 100000 parasites/ul)"),
         sudium_dys0_d0 = case_when(is.na(sudium_d0) ~ NA_character_,
                                   sudium_d0 <= 145 & sudium_d0 >= 134 & age <= 70 ~ "No",
                                   TRUE ~ "Yes"),
         potas_dys0_d0 = case_when(is.na(potas_d0) ~ NA_character_,
                                  potas_d0 <= 5.5 & potas_d0 >= 3.5 & age <= 70 ~ "No",
                                  TRUE ~ "Yes"),
         chlorid_dys0_d0 = case_when(is.na(chlorid_d0) ~ NA_character_,
                                    chlorid_d0 <= 107 & chlorid_d0 >= 98 & age <= 70 ~ "No",
                                    TRUE ~ "Yes"),
         calsum_dys0_d0 = case_when(is.na(calsum_d0) ~ NA_character_,
                                   calsum_d0 <= 2.75 & calsum_d0 >= 2.05 & age <= 70 ~ "No",
                                   TRUE ~ "Yes"),
         magnes_dys0_d0 = case_when(is.na(magnes_d0) ~ NA_character_,
                                   magnes_d0 <= 1.27 & magnes_d0 >= 0.55 & age <= 6 ~ "No",
                                   magnes_d0 <= 1.27 & magnes_d0 >= 0.65 & age <= 12 & age > 6 ~ "No",
                                   magnes_d0 <= 1.27 & magnes_d0 >= 0.7 & age <= 70 & age > 12 ~ "No",
                                   TRUE ~ "Yes"),
         anyelec_dys_d0 = case_when(is.na(sudium_dys0_d0) & is.na(potas_dys0_d0) & is.na(chlorid_dys0_d0) &
                                       is.na(calsum_dys0_d0) & is.na(magnes_dys0_d0) ~ NA_character_,
                                     sudium_dys0_d0 == "Yes" | potas_dys0_d0 == "Yes" | chlorid_dys0_d0 == "Yes" |
                                       calsum_dys0_d0 == "Yes" | magnes_dys0_d0  == "Yes" ~ "Yes",
                                     TRUE ~ "No"),
         sudium_dys0_d14 = case_when(is.na(sudium_d14) ~ NA_character_,
                                    sudium_d14 <= 145 & sudium_d14 >= 134 & age <= 70 ~ "No",
                                    TRUE ~ "Yes"),
         potas_dys0_d14 = case_when(is.na(potas_d14) ~ NA_character_,
                                   potas_d14 <= 5.5 & potas_d14 >= 3.5 & age <= 70 ~ "No",
                                   TRUE ~ "Yes"),
         chlorid_dys0_d14 = case_when(is.na(chlorid_d14) ~ NA_character_,
                                     chlorid_d14 <= 107 & chlorid_d14 >= 98 & age <= 70 ~ "No",
                                     TRUE ~ "Yes"),
         calsum_dys0_d14 = case_when(is.na(calsum_d14) ~ NA_character_,
                                    calsum_d14 <= 2.75 & calsum_d14 >= 2.05 & age <= 70 ~ "No",
                                    TRUE ~ "Yes"),
         magnes_dys0_d14 = case_when(is.na(magnes_d14) ~ NA_character_,
                                    magnes_d14 <= 1.27 & magnes_d14 >= 0.55 & age <= 6 ~ "No",
                                    magnes_d14 <= 1.27 & magnes_d14 >= 0.65 & age <= 12 & age > 6 ~ "No",
                                    magnes_d14 <= 1.27 & magnes_d14 >= 0.7 & age <= 70 & age > 12 ~ "No",
                                    TRUE ~ "Yes"),
         anyelec_dys_d14 = case_when(is.na(sudium_dys0_d14) & is.na(potas_dys0_d14) & is.na(chlorid_dys0_d14) &
                                        is.na(calsum_dys0_d14) & is.na(magnes_dys0_d14) ~ NA_character_,
                                      sudium_dys0_d14 == "Yes" | potas_dys0_d14 == "Yes" | chlorid_dys0_d14 == "Yes" |
                                        calsum_dys0_d14 == "Yes" | magnes_dys0_d14  == "Yes" ~ "Yes",
                                      TRUE ~ "No"),
         sudium_dys0_d28 = case_when(is.na(sudium_d28) ~ NA_character_,
                                    sudium_d28 <= 145 & sudium_d28 >= 134 & age <= 70 ~ "No",
                                    TRUE ~ "Yes"),
         potas_dys0_d28 = case_when(is.na(potas_d28) ~ NA_character_,
                                   potas_d28 <= 5.5 & potas_d28 >= 3.5 & age <= 70 ~ "No",
                                   TRUE ~ "Yes"),
         chlorid_dys0_d28 = case_when(is.na(chlorid_d28) ~ NA_character_,
                                     chlorid_d28 <= 107 & chlorid_d28 >= 98 & age <= 70 ~ "No",
                                     TRUE ~ "Yes"),
         calsum_dys0_d28 = case_when(is.na(calsum_d28) ~ NA_character_,
                                    calsum_d28 <= 2.75 & calsum_d28 >= 2.05 & age <= 70 ~ "No",
                                    TRUE ~ "Yes"),
         magnes_dys0_d28 = case_when(is.na(magnes_d28) ~ NA_character_,
                                    magnes_d28 <= 1.27 & magnes_d28 >= 0.55 & age <= 6 ~ "No",
                                    magnes_d28 <= 1.27 & magnes_d28 >= 0.65 & age <= 12 & age > 6 ~ "No",
                                    magnes_d28 <= 1.27 & magnes_d28 >= 0.7 & age <= 70 & age > 12 ~ "No",
                                    TRUE ~ "Yes"),
         anyelec_dys_d28 = case_when(is.na(sudium_dys0_d28) & is.na(potas_dys0_d28) & is.na(chlorid_dys0_d28) &
                                        is.na(calsum_dys0_d28) & is.na(magnes_dys0_d28) ~ NA_character_,
                                      sudium_dys0_d28 == "Yes" | potas_dys0_d28 == "Yes" | chlorid_dys0_d28 == "Yes" |
                                        calsum_dys0_d28 == "Yes" | magnes_dys0_d28 == "Yes" ~ "Yes",
                                      TRUE ~ "No"),
         sudium_dys_d0 = case_when(is.na(sudium_d0) ~ NA_character_,
                                    sudium_d0 > 145 ~ "Hypernatremia",
                                    sudium_d0 < 134 ~ "hyponatremia",
                                   TRUE ~ "Normal"),
         potas_dys_d0 = case_when(is.na(potas_d0) ~ NA_character_,
                                   potas_d0 > 5.5 ~ "hyperkalemia",
                                   potas_d0 < 3.5 ~ "hypokalemia",
                                   TRUE ~ "Normal"),
         chlorid_dys_d0 = case_when(is.na(chlorid_d0) ~ NA_character_,
                                    chlorid_d0 > 107 ~ "hyperchloremia",
                                    chlorid_d0 < 98 ~ "hypochloremia",
                                    TRUE ~ "Normal"),
         calsum_dys_d0 = case_when(is.na(calsum_d0) ~ NA_character_,
                                    calsum_d0 > 2.75 ~ "hypercalcemia",
                                    calsum_d0 < 2.05 ~ "hypocalcemia",
                                    TRUE ~ "Normal"),
         magnes_dys_d0 = case_when(is.na(magnes_d0) ~ NA_character_,
                                    magnes_d0 > 1.27  ~ "hypermagnesemia",
                                    magnes_d0 < 0.55 & age <= 6 ~ "hypomagnesemia",
                                    magnes_d0 < 0.65 & age <= 12 & age > 6 ~ "hypomagnesemia",
                                    magnes_d0 < 0.7 & age <= 70 & age > 12 ~ "hypomagnesemia",
                                    TRUE ~ "Normal"),
         sudium_dys_d14 = case_when(is.na(sudium_d14) ~ NA_character_,
                                    sudium_d14 > 145 ~ "Hypernatremia",
                                    sudium_d14 < 134 ~ "hyponatremia",
                                    TRUE ~ "Normal"),
         potas_dys_d14 = case_when(is.na(potas_d14) ~ NA_character_,
                                   potas_d14 > 5.5 ~ "hyperkalemia",
                                   potas_d14 < 3.5 ~ "hypokalemia",
                                   TRUE ~ "Normal"),
         chlorid_dys_d14 = case_when(is.na(chlorid_d14) ~ NA_character_,
                                     chlorid_d14 > 107 ~ "hyperchloremia",
                                     chlorid_d14 < 98 ~ "hypochloremia",
                                     TRUE ~ "Normal"),
         calsum_dys_d14 = case_when(is.na(calsum_d14) ~ NA_character_,
                                    calsum_d14 > 2.75 ~ "hypercalcemia",
                                    calsum_d14 < 2.05 ~ "hypocalcemia",
                                    TRUE ~ "Normal"),
         magnes_dys_d14 = case_when(is.na(magnes_d14) ~ NA_character_,
                                    magnes_d14 > 1.27  ~ "hypermagnesemia",
                                    magnes_d14 < 0.55 & age <= 6 ~ "hypomagnesemia",
                                    magnes_d14 < 0.65 & age <= 12 & age > 6 ~ "hypomagnesemia",
                                    magnes_d14 < 0.7 & age <= 70 & age > 12 ~ "hypomagnesemia",
                                    TRUE ~ "Normal"),
         sudium_dys_d28 = case_when(is.na(sudium_d28) ~ NA_character_,
                                    sudium_d28 > 145 ~ "Hypernatremia",
                                    sudium_d28 < 134 ~ "hyponatremia",
                                    TRUE ~ "Normal"),
         potas_dys_d28 = case_when(is.na(potas_d28) ~ NA_character_,
                                   potas_d28 > 5.5 ~ "hyperkalemia",
                                   potas_d28 < 3.5 ~ "hypokalemia",
                                   TRUE ~ "Normal"),
         chlorid_dys_d28 = case_when(is.na(chlorid_d28) ~ NA_character_,
                                     chlorid_d28 > 107 ~ "hyperchloremia",
                                     chlorid_d28 < 98 ~ "hypochloremia",
                                     TRUE ~ "Normal"),
         calsum_dys_d28 = case_when(is.na(calsum_d28) ~ NA_character_,
                                    calsum_d28 > 2.75 ~ "hypercalcemia",
                                    calsum_d28 < 2.05 ~ "hypocalcemia",
                                    TRUE ~ "Normal"),
         magnes_dys_d28 = case_when(is.na(magnes_d28) ~ NA_character_,
                                    magnes_d28 > 1.27  ~ "hypermagnesemia",
                                    magnes_d28 < 0.55 & age <= 6 ~ "hypomagnesemia",
                                    magnes_d28 < 0.65 & age <= 12 & age > 6 ~ "hypomagnesemia",
                                    magnes_d28 < 0.7 & age <= 70 & age > 12 ~ "hypomagnesemia",
                                    TRUE ~ "Normal")
         )

data_d0 <-
  dataset %>% 
  select(record_id, study, study_year, pid, sex , yob, age, age_group, weight, height, bmi, bmi_cat, parasitemia = malaria_d0,
         parasitemia_cat = plasmod_0, malaria_status = malaria_0, sudium = sudium_d0, potas = potas_d0, chlorid = chlorid_d0,
         calsum = calsum_d0, magnes = magnes_d0, sudium_dys = sudium_dys_d0, potas_dys = potas_dys_d0,
         chlorid_dys = chlorid_dys_d0, calsum_dys = calsum_dys_d0, magnes_dys = magnes_dys_d0, anyelec_dys = anyelec_dys_d0) %>% 
  filter(!is.na(anyelec_dys)) %>% 
  mutate(follow_up = "D0")

data_d14 <-
  dataset %>% 
  select(record_id, study, study_year, pid, sex , yob, age, age_group, weight, height, bmi, bmi_cat, parasitemia = malaria_d14,
         parasitemia_cat = plasmod_14, malaria_status = malaria_14, sudium = sudium_d14, potas = potas_d14, chlorid = chlorid_d14,
         calsum = calsum_d14, magnes = magnes_d14, sudium_dys = sudium_dys_d14, potas_dys = potas_dys_d14,
         chlorid_dys = chlorid_dys_d14, calsum_dys = calsum_dys_d14, magnes_dys = magnes_dys_d14, anyelec_dys = anyelec_dys_d14) %>% 
  filter(!is.na(anyelec_dys)) %>% 
  mutate(follow_up = "D14")

data_d28 <-
  dataset %>% 
  select(record_id, study, study_year, pid, sex , yob, age, age_group, weight, height, bmi, bmi_cat, parasitemia = malaria_d28,
         parasitemia_cat = plasmod_28, malaria_status = malaria_28, sudium = sudium_d28, potas = potas_d28, chlorid = chlorid_d28,
         calsum = calsum_d28, magnes = magnes_d28, sudium_dys = sudium_dys_d28, potas_dys = potas_dys_d28,
         chlorid_dys = chlorid_dys_d28, calsum_dys = calsum_dys_d28, magnes_dys = magnes_dys_d28, anyelec_dys = anyelec_dys_d28) %>% 
  filter(!is.na(anyelec_dys)) %>% 
  mutate(follow_up = "D28")


database <-
  data_d0 %>% 
  bind_rows(data_d14) %>% 
  bind_rows(data_d28) %>% 
  mutate(stratum = case_when(malaria_status == "Positive" ~ "Cases",
                             malaria_status == "Negative" ~ "Controls"),
         log_parasitemia = case_when(parasitemia > 0 ~ log2(parasitemia),
                                     TRUE ~ parasitemia)) %>% 
  mutate_at(vars(age_group, bmi_cat, sudium_dys, potas_dys, chlorid_dys, calsum_dys , magnes_dys),
            ~(ifelse(is.na(.), "Unknown", .)))

## table 1 ---------------------------------------------------------------------
tab_01 <-
  database %>% 
  count(follow_up) %>% 
  mutate(name = "Number of participants", value = NA_character_) %>%  
  pivot_wider(names_from = follow_up, values_from = n)
  

tab_02 <-
  database %>% 
  select(record_id, follow_up, anyelec_dys, malaria_status) %>% 
  pivot_longer(c("anyelec_dys", "malaria_status")) %>% 
  count(follow_up, name, value) %>% 
  pivot_wider(names_from = follow_up, values_from = n)

tab_1 <-
  tab_01 %>% 
  bind_rows(tab_02) %>% 
  mutate(Total = rowSums(select(., 3 : 5), na.rm = T),
         name = case_when(str_detect(name, "elec_") ~ "Any electrolyte disbalance",
                          str_detect(name, "mal") ~ "Malaria infection",
                          TRUE ~ name),
         name = unfill_vec(name))

## table 2 ---------------------------------------------------------------------

tab2_01 <- 
  database %>% 
  count(stratum) %>% 
  pivot_wider(names_from = stratum, values_from = n) %>%
  mutate(total = rowSums(select(., 1 : 2), na.rm = T),
         label = " ", levels = "All", p = NA_character_) %>% 
  select(4, 5, 3, 1, 2, 6) %>% 
  mutate_all(~(as.character(.)))

tab2_02 <-
  database %>%
  rename(anyelecdys = anyelec_dys) %>%
  select(record_id, stratum, sex, age_group, bmi_cat, parasitemia_cat, sudium_dys, potas_dys,
         chlorid_dys, calsum_dys, magnes_dys, anyelecdys) %>%
  pivot_longer(-c(record_id, stratum), names_to = "label", values_to = "levels") %>%
  group_by(label, levels) %>%
  summarise(total = n())

exp_1 = c("age_group", "bmi_cat", "parasitemia_cat", "sex", "sudium_dys", "potas_dys",
          "chlorid_dys", "calsum_dys", "magnes_dys", "anyelecdys")

tab2_03 <- 
  database %>% 
  rename(anyelecdys = anyelec_dys) %>% 
  mutate_at(vars(ends_with("_dys")), ~(relevel(factor(.), ref = "Normal"))) %>% 
  summary_factorlist("stratum", exp_1, p = TRUE, digits = c(1, 1, 3, 0)) %>% 
  mutate_all(~(ifelse(. == "", NA_character_, .))) %>% 
  fill(label) %>% 
  left_join(tab2_02, by = c("label", "levels")) %>% 
  select(1, 2, 6, 3 : 5) %>% 
  arrange(factor(levels, levels = c("0-6", "7-12", "13-70", "Normal Weight 18.5-24.9", "Overweight 25-29.9", "Obesity > 30", "Underweight < 18.5",
                                    "High parasitaemia (> 100000 parasites/ul)", "Moderatly high parasitaemia (10001-100000 parasites/ul)",
                                    "Medium parasitaemia (1001-10000 parasites/ul)", "Low parasitaemia (1-1000 parasites/ul)", "No parasitemia"))) %>% 
  arrange(factor(label, levels = c("age_group", "bmi_cat", "parasitemia_cat", "sex", "sudium_dys", "potas_dys", "chlorid_dys",
                                   "calsum_dys", "magnes_dys", "anyelec_dys"))) %>% 
  mutate(label = case_when(label == "age_group" ~ "Age",
                           label == "sex" ~ "Gender",
                           label == "bmi_cat" ~ "Weight status",
                           label == "parasitemia_cat" ~ "Parasitemia",
                           label == "sudium_dys" ~ "Sodium",
                           label == "potas_dys" ~ "Potassium",
                           label == "calsum_dys" ~ "Calcium",
                           label == "magnes_dys" ~ "Magnesium",
                           label == "chlorid_dys" ~ "Chloride",
                           label == "anyelecdys" ~ "Any electrolyte disbalance"),
         label = unfill_vec(label)) %>% 
  mutate_at(vars(Cases, Controls), ~(str_replace(., "\\)", "%)"))) %>% 
  mutate_all(~(as.character(.)))

tab_2 <-
  tab2_01 %>% 
  bind_rows(tab2_03)

## table 3 ---------------------------------------------------------------------
exp_2 = c("age", "bmi", "parasitemia", "sudium", "potas", "calsum", "magnes", "chlorid")

tab3_01 <- 
  database %>% 
  summary_factorlist("stratum", exp_2, p = TRUE, cont = "median", digits = c(0, 0, 3, 1)) %>% 
  mutate(label = case_when(label == "age" ~ "Age in years",
                           label == "bmi" ~ "BMI",
                           label == "parasitemia" ~ "Parasitaemia",
                           label == "sudium" ~ "Sodium, mmol/l",
                           label == "potas" ~ "Potassium, mmol/l",
                           label == "calsum" ~ "Calcium, mmol/l",
                           label == "magnes" ~ "Magnesium, mmol/l",
                           label == "chlorid" ~ "Chloride, mmol/l"))

tab3_02 <- 
  database %>% 
  summary_factorlist("stratum", exp_2, p = TRUE, digits = c(0, 0, 3, 1)) %>% 
  mutate(label = case_when(label == "age" ~ "Age in years",
                           label == "bmi" ~ "BMI",
                           label == "parasitemia" ~ "Parasitaemia",
                           label == "sudium" ~ "Sodium, mmol/l",
                           label == "potas" ~ "Potassium, mmol/l",
                           label == "calsum" ~ "Calcium, mmol/l",
                           label == "magnes" ~ "Magnesium, mmol/l",
                           label == "chlorid" ~ "Chloride, mmol/l"))

tab3_03 <- 
  database %>% 
  pivot_longer(all_of(exp_2)) %>% 
  group_by(name, stratum) %>% 
  summarise(min_max = paste0("(", signif(min(value, na.rm = T), 2), " ; ", signif(max(value, na.rm = T), 2), ")")) %>% 
  ungroup() %>% 
  pivot_wider(names_from = stratum, values_from = min_max) %>% 
  rename(label = name) %>% 
  mutate(label = case_when(label == "age" ~ "Age in years",
                           label == "bmi" ~ "BMI",
                           label == "parasitemia" ~ "Parasitaemia",
                           label == "sudium" ~ "Sodium, mmol/l",
                           label == "potas" ~ "Potassium, mmol/l",
                           label == "calsum" ~ "Calcium, mmol/l",
                           label == "magnes" ~ "Magnesium, mmol/l",
                           label == "chlorid" ~ "Chloride, mmol/l"),
         levels = "(Min ; Max)",
         p = "") %>% 
  relocate(levels, .after = label)

tab_3 <- 
  tab3_01 %>% 
  bind_rows(tab3_02) %>% 
  bind_rows(tab3_03) %>% 
  arrange(factor(label, levels = c("Age in years", "BMI", "Parasitaemia", "Sodium, mmol/l", "Potassium, mmol/l", "Calcium, mmol/l",
                                   "Magnesium, mmol/l", "Chloride, mmol/l"))) %>% 
  mutate(label = unfill_vec(label))

## table 4 ---------------------------------------------------------------------
tab4_01 <- 
  database %>% 
  count(anyelec_dys) %>% 
  pivot_wider(names_from = anyelec_dys, values_from = n) %>%
  mutate(total = rowSums(select(., 1 : 2), na.rm = T),
         label = " ", levels = "All", p = NA_character_) %>% 
  select(4, 5, 3, 1, 2, 6) %>% 
  mutate_all(~(as.character(.)))

tab4_02 <-
  database %>%
  select(record_id, stratum, sex, age_group, bmi_cat, parasitemia_cat, anyelec_dys) %>%
  pivot_longer(-c(record_id, anyelec_dys), names_to = "label", values_to = "levels") %>%
  group_by(label, levels) %>%
  summarise(total = n())


exp_3 = c("age_group", "bmi_cat", "parasitemia_cat", "sex", "stratum")

tab4_03 <- 
  database %>% 
  summary_factorlist("anyelec_dys", exp_3, p = TRUE, digits = c(1, 1, 3, 0)) %>% 
  mutate_all(~(ifelse(. == "", NA_character_, .))) %>% 
  fill(label) %>% 
  left_join(tab4_02, by = c("label", "levels")) %>% 
  select(1, 2, 6, 3 : 5) %>% 
  arrange(factor(levels, levels = c("0-6", "7-12", "13-70", "Normal Weight 18.5-24.9", "Overweight 25-29.9", "Obesity > 30", "Underweight < 18.5",
                                    "High parasitaemia (> 100000 parasites/ul)", "Moderatly high parasitaemia (10001-100000 parasites/ul)",
                                    "Medium parasitaemia (1001-10000 parasites/ul)", "Low parasitaemia (1-1000 parasites/ul)", "No parasitemia"))) %>% 
  arrange(factor(label, levels = c("age_group", "bmi_cat", "parasitemia_cat", "sex"))) %>% 
  mutate(label = case_when(label == "age_group" ~ "Age",
                           label == "sex" ~ "Gender",
                           label == "bmi_cat" ~ "Weight status",
                           label == "parasitemia_cat" ~ "Parasitemia",
                           label == "stratum" ~ "Malaria"),
         levels = case_when(levels == "Cases" ~ "Positive", levels == "Controls" ~ "Negative", T ~ levels),
         label = unfill_vec(label)) %>% 
  mutate_at(vars(No, Yes), ~(str_replace(., "\\)", "%)"))) %>% 
  mutate_all(~(as.character(.)))

tab_4 <-
  tab4_01 %>% 
  bind_rows(tab4_03)


## table Linear Regression------------------------------------------------------
database_reg <-
  database %>% 
  mutate(sudium_below = if_else(sudium < 134, "Cases", "Controls"),
         potas_below = if_else(potas < 3.5, "Cases", "Controls"),
         chlorid_below = if_else(chlorid < 98, "Cases", "Controls"),
         calsum_below = if_else(calsum < 2.05, "Cases", "Controls"),
         magnes_below = if_else(magnes < 0.65 & age <= 12 & age > 6 | magnes < 0.7 & age <= 70 & age > 12, "Cases", "Controls"),
         sudium_above = if_else(sudium > 145, "Cases", "Controls"),
         potas_above = if_else(potas > 5.5, "Cases", "Controls"),
         chlorid_above = if_else(chlorid > 107, "Cases", "Controls"),
         calsum_above = if_else(calsum > 2.75, "Cases", "Controls"),
         magnes_above = if_else(magnes > 1.27 | magnes < 0.55 & age <= 6, "Cases", "Controls")) %>% 
  mutate_at(vars(sudium_below : magnes_above), ~(factor(., levels = c("Cases", "Controls"), labels = c("Cases", "Controls"))))


# table 5 ----------------------------------------------------------------------
mod.below_01 <- glm(sudium_below ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg)
below_01 <- tbl_regression(mod.below_01, exponentiate = TRUE)

mod.below_02 <- glm(potas_below ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg)
below_02 <- tbl_regression(mod.below_02, exponentiate = TRUE)

mod.below_03 <- glm(chlorid_below ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg)
below_03 <- tbl_regression(mod.below_03, exponentiate = TRUE)

mod.below_04 <- glm(calsum_below ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg_2)
below_04 <- tbl_regression(mod.below_04, exponentiate = TRUE)

mod.below_05 <- glm(magnes_below ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg)
below_05 <- tbl_regression(mod.below_05, exponentiate = TRUE)


tab.below <-
  tbl_merge(tbls = list(below_01, below_02, below_03, below_04, below_05), 
            tab_spanner = c("**Sodium**", "**Potasium**", "**Chlorid**", "**Calcium**", "**Magnesium**"))

h_below <- tab.below[1]

tab_5 <-
  as.data.frame(h_below[['table_body']]) %>% 
  filter(!is.na(term_1)) %>% 
  select(label, estimate_1, ci_1, p.value_1, estimate_2, ci_2, p.value_2, estimate_3, ci_3, p.value_3,
         estimate_4, ci_4, p.value_4, estimate_5, ci_5, p.value_5) %>%
  mutate_at(vars(contains("p.value")), list(~ifelse(. == "NA", NA, .))) %>% 
  mutate_at(vars(contains("estimate")), list(~ round(as.numeric(.), 2))) %>% 
  mutate_at(vars(contains("p.value")), ~(ifelse(. <= 0.001, "< 0.001", as.character(round(., 2))))) %>% 
  arrange(factor(label, levels = c("No parasitemia", "Low parasitaemia (1-1000 parasites/ul)", "Medium parasitaemia (1001-10000 parasites/ul)",
                                   "Moderatly high parasitaemia (10001-100000 parasites/ul)", "High parasitaemia (> 100000 parasites/ul)")))

# table 6 ----------------------------------------------------------------------
mod.above_01 <- glm(sudium_above ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg)
above_01 <- tbl_regression(mod.above_01, exponentiate = TRUE)

mod.above_02 <- glm(potas_above ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg)
above_02 <- tbl_regression(mod.above_02, exponentiate = TRUE)

mod.above_03 <- glm(chlorid_above ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg)
above_03 <- tbl_regression(mod.above_03, exponentiate = TRUE)

mod.above_04 <- glm(calsum_above ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg)
above_04 <- tbl_regression(mod.above_04, exponentiate = TRUE)

mod.above_05 <- glm(magnes_above ~ relevel(factor(parasitemia_cat), ref = "No parasitemia"), family = binomial, data = database_reg)
above_05 <- tbl_regression(mod.above_05, exponentiate = TRUE)

tab.above <-
  tbl_merge(tbls = list(above_01, above_02, above_03, above_04, above_05), 
            tab_spanner = c("**Sodium**", "**Potasium**", "**Chlorid**", "**Calcium**", "**Magnesium**"))

h_above <- tab.above[1]

tab_6 <-
  as.data.frame(h_above[['table_body']]) %>% 
  filter(!is.na(term_1)) %>% 
  select(label, estimate_1, ci_1, p.value_1, estimate_2, ci_2, p.value_2, estimate_3, ci_3, p.value_3,
         estimate_4, ci_4, p.value_4, estimate_5, ci_5, p.value_5) %>%
  mutate_at(vars(contains("p.value")), list(~ifelse(. == "NA", NA, .))) %>% 
  mutate_at(vars(contains("estimate")), list(~ round(as.numeric(.), 2))) %>% 
  mutate_at(vars(contains("p.value")), ~(ifelse(. <= 0.001, "< 0.001", as.character(round(., 2))))) %>% 
  arrange(factor(label, levels = c("No parasitemia", "Low parasitaemia (1-1000 parasites/ul)", "Medium parasitaemia (1001-10000 parasites/ul)",
                                   "Moderatly high parasitaemia (10001-100000 parasites/ul)", "High parasitaemia (> 100000 parasites/ul)")))
 

# tab_6 <-
#   database %>% 
#   select(record_id, parasitemia, log_parasitemia, sudium, potas, chlorid, calsum, magnes) %>% 
#   pivot_longer(c("sudium", "potas", "chlorid", "calsum", "magnes")) %>% 
#   filter(!is.na(value), parasitemia > 0) %>% 
#   group_by(name) %>% 
#   summarize(cor = stats::cor.test(parasitemia, value, method = "pearson", alternative = "two.sided")$estimate,
#             pval = stats::cor.test(parasitemia, value, method = "pearson", alternative = "two.sided")$p.value,
#             cor2 = stats::cor.test(log_parasitemia, value, method = "pearson", alternative = "two.sided")$estimate,
#             pval2 = stats::cor.test(log_parasitemia, value, method = "pearson", alternative = "two.sided")$p.value) %>%
#   ungroup() %>% 
#   mutate(cor = round(cor, 2),
#          cor2 = round(cor2, 2),
#          pval = if_else(pval < 0.001, "<0.001", as.character(round(pval, 3))),
#          pval2 = if_else(pval2 < 0.001, "<0.001", as.character(round(pval2, 3))),
#          name = case_when(str_detect(name, "sudium") ~ "Sodium",
#                           str_detect(name, "potas") ~ "Potassium",
#                           str_detect(name, "calsum") ~ "Calcium",
#                           str_detect(name, "magnes") ~ "Magnesium",
#                           str_detect(name, "chlorid") ~ "Chloride"))

## table 7 Logistic Regression -------------------------------------------------
data_join <- tribble(
  ~variable,  ~value,
  "sudium_dys", "Sodium",
  "1", "Normal",
  "2", "Hypernatremia",
  "3", "Hyponatremia",
  "potas_dys", "Potassium",
  "1", "Normal",
  "2", "Hyperkalemia",
  "3", "Hypokalemia",
  "chlorid_dys", "Chloride",
  "1", "Normal",
  "2", "Hyperchloremia",
  "3", "Hypochloremia",
  "calsum_dys", "Calcium",
  "1", "Normal",
  "2", "Hypercalcemia",
  "3", "Hypocalcemia",
  "magnes_dys", "Magnesium",
  "1", "Normal",
  "2", "Hypermagnesemia",
  "3", "Hypomagnesemia"
)


tr <- database %>% 
  select(stratum, sudium_dys, potas_dys, chlorid_dys, calsum_dys, magnes_dys) %>%
  mutate_all(~(str_to_lower(.))) %>% 
  mutate(stratum = factor(stratum, levels = c("cases", "controls"), labels = c("cases", "controls"))) %>% 
  mutate_at(vars(2 : 6), ~(case_when(str_detect(., "normal") ~ "1",
                                     str_detect(., "hyper") ~ "2",
                                     str_detect(., "hypo") ~ "3")))

### Chunk 3 --------------------------------------------------------------------
g1 <- tbl_summary(
  tr,
  by = stratum, # split table by group
  missing = "no"# don't list missing data separately
) %>%
  add_n() %>% # add column with total number of non-missing observations
  add_p() %>% #pvalue_fun = function(x) sprintf(x, fmt='%#.10f')) %>% # test for a difference between groups
  modify_header(label = "**Variable**") %>% # update the column header
  bold_labels()


mod2 <- glm(stratum ~ sudium_dys + potas_dys + chlorid_dys + calsum_dys + magnes_dys, family = binomial, data = tr)

g2 <- tbl_regression(mod2, exponentiate = TRUE)

### Chunk 5 --------------------------------------------------------------------
tt <-
  tbl_merge(tbls = list(g1, g2), 
            tab_spanner = c("**Analysis univariable**" , "**Logistic regression**"))

h <- tt[1]

tab_7 <-
  as.data.frame(h[['table_body']]) %>% 
  select(Variable = label, stat_1_1, stat_2_1, p.value_1, N_2, estimate_2,
         estimate_2, ci_2, p.value_2) %>%
  mutate_at(vars(contains("p.value")), list(~ifelse(. == "NA", NA, .))) %>% 
  mutate_at(vars(contains("p.value")), list(~ifelse(. <= 0.001, "< 0.001", as.character(round(., 2))))) %>% 
  mutate_at(vars(contains("estimate")), list(~ round(as.numeric(.), 2))) %>% 
  mutate_at(vars(stat_1_1:p.value_2), list(~ifelse(is.na(.), "-", .))) %>% 
  select(-N_2) %>% 
  bind_cols(data_join) %>% 
  select(9, 2 : 7)


## Figure 1 --------------------------------------------------------------------

fig_1a <-
  database  %>% 
  select(record_id, parasitemia, sudium, potas, chlorid, calsum, magnes) %>% 
  pivot_longer(c("sudium", "potas", "chlorid", "calsum", "magnes")) %>% 
  mutate(name = case_when(name == "sudium" ~ "Sodium",
                          name == "potas" ~ "Potassium",
                          name == "calsum" ~ "Calcium",
                          name == "magnes" ~ "Magnesium",
                          name == "chlorid" ~ "Chloride")) %>% 
  ggplot(aes(value, parasitemia, color = name)) + 
  geom_point() +
  theme_classic() +
  # stat_cor(method = "pearson") +
  facet_wrap(~name, scales = "free_x") +
  labs(color = "") +
  theme(legend.position = "none")

fig_1b <-
  database  %>% 
  select(record_id, parasitemia, sudium, potas, chlorid, calsum, magnes) %>% 
  pivot_longer(c("sudium", "potas", "chlorid", "calsum", "magnes")) %>% 
  mutate(name = case_when(name == "sudium" ~ "Sodium",
                          name == "potas" ~ "Potassium",
                          name == "calsum" ~ "Calcium",
                          name == "magnes" ~ "Magnesium",
                          name == "chlorid" ~ "Chloride")) %>% 
  ggplot(aes(value, parasitemia, color = name)) + 
  geom_point() +
  theme_classic() +
  # stat_cor(method = "pearson") +
  facet_wrap(~name, scales = "free_x") +
  scale_y_continuous(trans = 'log2') +
  labs(color = "",
       y = "Parasitemia (log2 transformation)") +
  theme(legend.position = "none")

fig_1c <-
  database  %>% 
  select(record_id, parasitemia, sudium, potas, chlorid, calsum, magnes) %>% 
  pivot_longer(c("sudium", "potas", "chlorid", "calsum", "magnes")) %>% 
  mutate(name = case_when(name == "sudium" ~ "Sodium",
                          name == "potas" ~ "Potassium",
                          name == "calsum" ~ "Calcium",
                          name == "magnes" ~ "Magnesium",
                          name == "chlorid" ~ "Chloride")) %>% 
  ggplot(aes(value, parasitemia, color = name)) + 
  geom_point() +
  theme_classic() +
  # stat_cor(method = "pearson") +
  facet_wrap(~name, scales = "free_x") +
  scale_y_continuous(trans = 'log2') +
  labs(color = "",
       y = "Parasitemia (log2 transformation)") +
  theme(legend.position = "none")


#Render ------------------------------------------------------------------------

reportfolder <- "reports/"
fileRmd <- "report_CalM2.Rmd"
outpath_DIR <- paste0(reportfolder, "Report_Stat_CalM_", format(Sys.Date(), "%Y-%m-%d"))
rmarkdown::render(fileRmd, output_file = outpath_DIR, quiet = TRUE)

# End --------------------------------------------------------------------------
