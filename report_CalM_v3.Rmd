---
title: "CALME report"
author: "Saidou MAHMOUDOU"
date: '`r format(Sys.time(), "%d %B, %Y")`'
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE, message = FALSE)
library(flextable)
library(magrittr)
```

\

```{r}
tab_1 %>% 
  rename_all(~(c(" ", "Status", "D0", "D14", "D28", "All"))) %>% 
  flextable() %>% 
  align(part = "all") %>% # left align
  set_caption(caption = "Total of participant") %>% 
  theme_booktabs() %>% # default theme
  autofit() %>% 
  line_spacing(space = 0.5, part = "all")%>% 
  bold(part = "header", j = c(" ", "Status", "D0", "D14", "D28", "All")) %>% 
  fit_to_width(7.5)
```

\

```{r}
tab_2 %>% 
  select(-levels) %>% 
  rename_all(~(c("Variable", "Malaria Negative", "Malaria Positive", "p-value"))) %>% 
  flextable() %>% 
  align(part = "all") %>% # left align
  set_caption(caption = "Comparison of electrolyte levels in malaria positive and malaria negative") %>% 
  theme_booktabs() %>% # default theme
  autofit() %>% 
  line_spacing(space = 0.5, part = "all")%>% 
  bold(part = "header", j = c("Variable", "Malaria Negative", "Malaria Positive", "p-value"))
```

\newpage

```{r}
tab_3 %>% 
  rename_all(~(c("Variable", " ", "Malaria Negative", "Malaria Positive", "p-value"))) %>% 
  flextable() %>% 
  align(part = "all") %>% # left align
  set_caption(caption = "Comparaison of electrolytes levels in different groups") %>% 
  theme_booktabs() %>% # default theme
  autofit() %>% 
  line_spacing(space = 0.5, part = "all")%>% 
  bold(part = "header", j = c("Variable", " ", "Malaria Negative", "Malaria Positive", "p-value"))
```

\

```{r}
tab_4 %>% 
  rename_all(~(c("Malaria status", "Na", "K", "Cl", "Ca", "Mg"))) %>% 
  flextable() %>% 
  align(part = "all") %>% # left align
  set_caption(caption = "Mean statistics of electrolyte disbalence in malaria participants") %>% 
  theme_booktabs() %>% # default theme
  autofit() %>% 
  line_spacing(space = 0.5, part = "all")%>% 
  bold(part = "header", j = c("Malaria status", "Na", "K", "Cl", "Ca", "Mg"))
```

\newpage

```{r}
tab_5 %>% 
  rename_all(~(c("Variable", " ", "Malaria Negative", "Malaria Positive", "p-value",
                 "Malaria Negative ", "Malaria Positive ", "p-value "))) %>% 
  flextable() %>% 
  align(align = "center", part = "all") %>% # left align
  set_caption(caption = "Gender distribution of electrolytes levels in different groups") %>% 
  theme_booktabs() %>% # default theme
  autofit() %>% 
  line_spacing(space = 0.5, part = "all")%>% 
  bold(part = "header", j = c("Variable", " ", "Malaria Negative", "Malaria Positive", "p-value",
                 "Malaria Negative ", "Malaria Positive ", "p-value ")) %>% 
  add_header_row(top = TRUE, values = c(" ", "Male", "Female"), colwidths = c(2,3,3)) %>% 
  fit_to_width(7.5)
```

\newpage

```{r fig.cap="Relationship between the real number of parasitaemias and the value of electrolytes", fig.pos='h'}
fig_1a
```

\

```{r fig.cap="Relationship between the log2 transformation of the number of parasitaemias and the value of electrolytes", fig.pos='h'}
fig_1b
```

